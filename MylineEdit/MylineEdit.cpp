#include "MylineEdit.h"
#include <qobject.h>
#include <QObject>
#include <qwidget.h>

MylineEdit::MylineEdit(QWidget *parent):QLineEdit(parent){
    parentwindow=parent;
}


void MylineEdit::focusInEvent(QFocusEvent *event){
    QLineEdit::focusInEvent(event);
    QLineEdit::selectAll();
}


