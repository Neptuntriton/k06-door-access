#ifndef MYLINEEDIT_H
#define MYLINEEDIT_H
#include <qlineedit.h>
#include <qstring.h>
#include <qmessagebox.h>
#include <qwidget.h>

class MylineEdit : public QLineEdit{
    Q_OBJECT

    public:
        MylineEdit(QWidget *parent);

    private:
        QString InputTextLabel;
        QWidget *parentwindow;

    void focusInEvent(QFocusEvent *event);         // Override

};

#endif // MYLINEEDIT_H
