#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QEvent>



MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow){
    std::cout << "K06 Location Access is up and running!" << std::endl;

    // zunächst killen wir die LED's
    LEDsSwitchOff();

    ui->setupUi(this);

    // wir bauen den Debugger
    cDeb::Init(0, NULL);
    cDeb::setConsoleMode(true);
//    cDeb::High("K06 Location Door Access is started!");


    // This for Debugging Purpose and gets hidden
    ui->ErrorView->hide();
    ui->InfoListView->hide();

    this->setWindowFlags(Qt::MSWindowsFixedSizeDialogHint);
    this->setWindowFlags(Qt::CustomizeWindowHint);
    this->setWindowFlags(Qt::FramelessWindowHint);
    this->statusBar()->hide();
    this->menuBar()->hide();
    this->resize(320,240);

    // Some Bla
    this->setWindowTitle("LocationAccess");

    // Instanzieren den UserManager
    MyUserManager = new LocationUserManager("RLbtjM5SrSDbneCXq", ui->UserNameComboBox);
    QObject::connect(MyUserManager, SIGNAL(OnAccess(LocationAccessObject*)), this, SLOT(ReactOnAccessEvent(LocationAccessObject*)));

    // optional um einiges anzuzeigen
    MyUserManager->AddListViewtoShowErrors(    ui->ErrorView);
    MyUserManager->AddListViewtoShowInfos(     ui->InfoListView);

    //Instanzieren des MylineEdits
    MyPassWordEdit = new MylineEdit(this);
    MyPassWordEdit->setGeometry(10,178,300,32);
    MyPassWordEdit->setEchoMode(MylineEdit::Password);
    MyPassWordEdit->installEventFilter(this);


    //Instanzieren des VirtualKeyboards
    keyboard = new QVirtualKeyboard(this);
    keyboard->setGeometry(10,5,300,150);
    keyboard->hide();

    // We are Loading all MovieLocations
    MyAnimationLocations = new QStringList();

    ReScanForAnimatedGifs();

    MyAnimationIndex       = 0;
    MyVideoLabel           = new cVideoLabel(this,MyAnimationLocations->value(MyAnimationIndex));
    MyVideoReloadTimer     = new QTimer(this);
    MyInfoLabelTimer       = new QTimer(this);
    MyGifFolderReScanTimer = new QTimer(this);
    // Alle 10 Sekunden Checken wir as Verzeichnis
    MyGifFolderReScanTimer->start(10000);


    MyRFIDReader            = new RFIDReader();

    // We are connecting all neccessary Events
    QObject::connect(MyVideoLabel,           SIGNAL(OnClick()),           this, SLOT(ReactOnVideoLabelClick()));
    QObject::connect(MyVideoReloadTimer,     SIGNAL(timeout()),           this, SLOT(ReactOnVideoReloadTimer()));
    QObject::connect(MyInfoLabelTimer  ,     SIGNAL(timeout()),           this, SLOT(ReactOnInfoLabelTimer()));
    QObject::connect(MyGifFolderReScanTimer, SIGNAL(timeout()),           this, SLOT(ReactOnGifFolderReScanTimer()));
    QObject::connect(keyboard->buttonC6,     SIGNAL(clicked()),           this, SLOT(ReactOnPushButtonClicked()));
    QObject::connect(MyRFIDReader,           SIGNAL(OnTagFound(QString)), this, SLOT(ReactOnNewTagFound(QString)));

}

MainWindow::~MainWindow(){
    delete ui;
    delete MyUserManager;
    delete MyRFIDReader;
}

void MainWindow::ReactOnVideoButtonClicked(){
    if (MyVideoLabel->isHidden() == true){
        ShowAnimation();
    } else {
        HideAnimation();
    }
}

void MainWindow::ReactOnNewTagFound(QString AnRFIDTag){
    HideAnimation();
    ui->InfoLabel->setText("RFID: " + AnRFIDTag.toUpper());
    MyRFIDAccess = MyUserManager->TryToGetAccessByRFID(AnRFIDTag.toUpper());
}

void MainWindow::ReactOnVideoLabelClick(){
    HideAnimation();
}

void MainWindow::ReactOnVideoReloadTimer(){
  ShowAnimation();
}

void MainWindow::ReactOnInfoLabelTimer(){
    ui->InfoLabel->setStyleSheet("QLabel { background-color : #F2F1F0; color : black; }");
    ui->InfoLabel->setText("K06 Door Access");
    MyInfoLabelTimer->stop();
}


void MainWindow::ReactOnGifFolderReScanTimer(){
    ReScanForAnimatedGifs();
}

// Wir laden die nächste Animation
void MainWindow::LoadNextAnimation(){
    // zunächst erhöhen wir den Index
    MyAnimationIndex += 1;
    MyAnimationIndex = MyAnimationIndex % MyAnimationLocations->count();
    // Dann laden wir die neue Animation
    MyVideoLabel->LoadNewAnimation(MyAnimationLocations->value(MyAnimationIndex));
}

void MainWindow::HideAnimation(){
    MyVideoLabel->hide();
    MyVideoReloadTimer->start(60000);
}

void MainWindow::ShowAnimation(){
    MyVideoReloadTimer->stop();
    LoadNextAnimation();
    MyVideoLabel->show();
}

void MainWindow::ReactOnPushButtonClicked(){

    /*
    MyUserManager = new LocationUserManager(3, ui->UserNameComboBox);
    MyUserManager->AddListViewtoShowErrors(    ui->ErrorView);
    MyUserManager->AddListViewtoShowInfos(     ui->InfoListView);
    */

    QString AUserName = ui->UserNameComboBox->currentText().split(" | ").at(0);
    QString APassword = MyPassWordEdit->text();
    MyUserAccess = MyUserManager->TryToGetAccessByUserNickNameAndPassword(AUserName, APassword);
    MyPassWordEdit->clear();
    keyboard->hide();
    ui->UserNameComboBox->setFocus();

    //MyRFIDAccess = MyUserManager->TryToGetAccessByRFID("042E8F41C12580");
    //MyRFIDAccess = MyUserManager->TryToGetAccessByRFID("042E8F41C12511");
    //MyUserAccess = MyUserManager->TryToGetAccessByUserNickNameAndPassword("Neptuntriton", "videoseven");
}

// We are scenning the AnimationsFolder For New Animated Gifs
void MainWindow::ReScanForAnimatedGifs(){
    QDir        *ADirectory  = new QDir("./Animations/");

    cDeb::Med("Scanning for gif Files.");
    cDeb::Med("|-> Folder: " + ADirectory->currentPath());

    QStringList *AFileList   = new QStringList(ADirectory->entryList(QStringList() << "*.gif", QDir::Files));
    cDeb::Med("|-> Count: " + QString::number(AFileList->count()));

    MyAnimationLocations->clear();
    for (int a = 0; a < AFileList->count(); a++ ){
        MyAnimationLocations->append("Animations/" + AFileList->value(a));
    }

    delete ADirectory;
    delete AFileList;
}



// wird aus gelöst wenn ein AccessEvent ausgelöst wird
void MainWindow::ReactOnAccessEvent(LocationAccessObject *AnAccessObject){
    // Wir müssen prüfen ob wir gefragt haben
    if (AnAccessObject == MyRFIDAccess){
        // Wir haben nach einer RFID gefragt
        if (AnAccessObject->HasAccess()){
            ui->InfoLabel->setStyleSheet("QLabel { background-color : #F7CF8F; color : #05BC00; }");
            ui->InfoLabel->setText("Access granted!");
            OpenDoor();
        } else {
            ui->InfoLabel->setStyleSheet("QLabel { background-color : grey; color : red; }");
            ui->InfoLabel->setText("Access denied!");
        }
        MyInfoLabelTimer->start(10000);
    }

    if (AnAccessObject == MyUserAccess){
        // Wir haben nach einem User gefragt
        if (AnAccessObject->HasAccess()){
            ui->InfoLabel->setStyleSheet("QLabel { font-style: bold; font-size: 30px ;background-color : #F7CF8F; color : #05BC00; }");
            ui->InfoLabel->setText("Access granted!");
            OpenDoor();
        } else {
            ui->InfoLabel->setStyleSheet("QLabel { font-style: bold; font-size: 30px ;background-color : grey; color : red; }");
            ui->InfoLabel->setText("Access denied!");
        }
        MyInfoLabelTimer->start(10000);
    }
}


// wir schalten alle LED's ab
void MainWindow::LEDsSwitchOff(){
    int led_fd;
    if ( (led_fd = open("/dev/leds", 0)) >= 0){
        std::cout << "|-> Switch Off LEDs" << std::endl;
        ioctl(led_fd, 0, 0);
        usleep(1000);
        ioctl(led_fd, 0, 1);
        usleep(1000);
        ioctl(led_fd, 0, 2);
        usleep(1000);
        ioctl(led_fd, 0, 3);
    }
}


// Wir klackern den Türöffner
void MainWindow::OpenDoor(){    
    int led_fd;    

    //aufbauen des Streams zur Led
    led_fd = open("/dev/leds0", 0);
    if (led_fd < 0) {
        led_fd = open("/dev/leds", 0);
    }

    if (led_fd < 0) {
        perror("open device leds");
        //exit(1);
    }

    for (int i = 0; i < 30; i++) {
        ioctl(led_fd, 1, 0);
        usleep(1000);
        ioctl(led_fd, 1, 1);
        usleep(1000);
        ioctl(led_fd, 1, 2);
        usleep(1000);
        ioctl(led_fd, 1, 3);
        usleep(25000);
        ioctl(led_fd, 0, 0);
        usleep(1000);
        ioctl(led_fd, 0, 1);
        usleep(1000);
        ioctl(led_fd, 0, 2);
        usleep(1000);
        ioctl(led_fd, 0, 3);
        usleep(25000);
    }
}

//Eventfilter um auf Tastenklick in PassWordEdit zu reagieren
bool MainWindow::eventFilter(QObject *obj, QEvent *event) {
     if (obj == MyPassWordEdit) {
         if (event->type() == QEvent::FocusIn) {             
             keyboard->show();
             return true;
         } else {
             return false;
         }
     }
}
