#-------------------------------------------------
#
# Project created by QtCreator 2012-10-18T13:57:12
#
#-------------------------------------------------

QT          += core gui network

TARGET       = LocationAccess
TEMPLATE     = app

#INCLUDEPATH += ./usr/include
#LIBS         += -L/lib/i386-linux-gnu -lusb-1.0

LIBS         += -L/usr/local/4.4.3/arm-none-linux-gnueabi/sys-root/lib/ -lusb-1.0


SOURCES     += main.cpp\
               mainwindow.cpp \
               LocationManager/LocationManager.cpp \
               VideoLabel/VideoLabel.cpp \
               QVirtualKeyboard/QVirtualKeyboard.cpp \
               MylineEdit/MylineEdit.cpp \
               ../../NeppisC++Libs/MD5/md5.cpp \
               ../../NeppisC++Libs/QJSON/serializerrunnable.cpp \
               ../../NeppisC++Libs/QJSON/serializer.cpp \
               ../../NeppisC++Libs/QJSON/qobjecthelper.cpp \
               ../../NeppisC++Libs/QJSON/parserrunnable.cpp \
               ../../NeppisC++Libs/QJSON/parser.cpp \
               ../../NeppisC++Libs/QJSON/json_scanner.cpp \
               ../../NeppisC++Libs/QJSON/json_parser.cc \
               ../../NeppisC++Libs/RFIDReader/touchatag_tag.cpp \
               ../../NeppisC++Libs/RFIDReader/touchatag_reader.cpp \
               ../../NeppisC++Libs/RFIDReader/RFIDClass.cpp \
               ../../NeppisC++Libs/Debug/Deb.cpp \
               ../../NeppisC++Libs/Debug/Debug.cpp

HEADERS     += mainwindow.h \
               LocationManager/LocationManager.h \
               VideoLabel/VideoLabel.h \
               QVirtualKeyboard/QVirtualKeyboard.h \
               MylineEdit/MylineEdit.h \
               ../../NeppisC++Libs/MD5/md5.h \
               ../../NeppisC++Libs/QJSON/stack.hh \
               ../../NeppisC++Libs/QJSON/serializerrunnable.h \
               ../../NeppisC++Libs/QJSON/serializer.h \
               ../../NeppisC++Libs/QJSON/qobjecthelper.h \
               ../../NeppisC++Libs/QJSON/qjson_export.h \
               ../../NeppisC++Libs/QJSON/qjson_debug.h \
               ../../NeppisC++Libs/QJSON/position.hh \
               ../../NeppisC++Libs/QJSON/parserrunnable.h \
               ../../NeppisC++Libs/QJSON/parser_p.h \
               ../../NeppisC++Libs/QJSON/parser.h \
               ../../NeppisC++Libs/QJSON/location.hh \
               ../../NeppisC++Libs/QJSON/json_scanner.h \
               ../../NeppisC++Libs/QJSON/json_parser.hh \
               ../../NeppisC++Libs/RFIDReader/touchatag_tag.h \
               ../../NeppisC++Libs/RFIDReader/touchatag_reader.h \
               ../../NeppisC++Libs/RFIDReader/RFIDClass.h \
               ../../NeppisC++Libs/Debug/Deb.h \
               ../../NeppisC++Libs/Debug/Debug.h

FORMS       += mainwindow.ui
