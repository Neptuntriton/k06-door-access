#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMovie>
#include <QEvent>
#include <fcntl.h>
#include <QObject>
#include <QMainWindow>
#include <sys/ioctl.h>
#include <QStringList>
#include <QApplication>
#include <VideoLabel/VideoLabel.h>
#include <MylineEdit/MylineEdit.h>
#include <../../NeppisC++Libs/Debug/Deb.h>
#include <../../NeppisC++Libs/RFIDReader/RFIDClass.h>
#include <LocationManager/LocationManager.h>
#include <QVirtualKeyboard/QVirtualKeyboard.h>


namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow{
    Q_OBJECT
    
    public:
        explicit MainWindow(QWidget *parent = 0);
        ~MainWindow();
    
    private:
        Ui::MainWindow      *ui;

        LocationUserManager  *MyUserManager;
        LocationAccessObject *MyRFIDAccess;
        LocationAccessObject *MyUserAccess;

        RFIDReader           *MyRFIDReader;

        cVideoLabel          *MyVideoLabel;

        MylineEdit           *MyPassWordEdit;

        QMovie               *MyAnimation;
        QStringList          *MyAnimationLocations;
        QTimer               *MyVideoReloadTimer;
        QTimer               *MyInfoLabelTimer;
        QTimer               *MyGifFolderReScanTimer;

        QVirtualKeyboard     *keyboard;

        int                   MyAnimationIndex;    // Switches between diffrent Movies in BackGround

        void                  OpenDoor();
        void                  LEDsSwitchOff();
        void                  HideAnimation();
        void                  ShowAnimation();
        void                  LoadNextAnimation();
        void                  ReScanForAnimatedGifs();


    protected:
        bool eventFilter(QObject *obj, QEvent *ev);

    private slots:
        void ReactOnPushButtonClicked();
        void ReactOnVideoButtonClicked();
        void ReactOnVideoLabelClick();
        void ReactOnVideoReloadTimer();
        void ReactOnInfoLabelTimer();
        void ReactOnGifFolderReScanTimer();
        void ReactOnNewTagFound(QString AnRFIDTag);
        void ReactOnAccessEvent(LocationAccessObject *AnAccessObject);
};

#endif // MAINWINDOW_H
