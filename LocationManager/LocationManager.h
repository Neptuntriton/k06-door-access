#include <QUrl>
#include <QTime>
#include <QTimer>
#include <QObject>
#include <QString>
#include <../../NeppisC++Libs/MD5/md5.h>
#include <QListView>
#include <QNetworkReply>
#include <QStandardItem>
#include <QFontComboBox>
#include <../../NeppisC++Libs/QJSON/parser.h>
#include <QNetworkRequest>
#include <QStandardItemModel>
#include <QNetworkAccessManager>

class LocationAccessObject :public QObject{
    Q_OBJECT
    private:
        bool                 MyAccessState;
        QNetworkReply       *MyNetWorkRepley;
        QString              MyRelatedPassword;
        QString              MyRelatedRFID;
        QString              MyRelatedUserNickName;

    public:
        bool                 HasAccess();
        QString              PassWord();
        QString              UserNickName();
        QString              RFID();
        QNetworkReply       *NetworkReply();
        LocationAccessObject(QString UserNickName, QString PassWord, QNetworkReply *NetWorkRepley);
        LocationAccessObject(QString RFID,                           QNetworkReply *NetWorkRepley);
        void                 ActivateAccess();
};

// We have to derive from QObject to use the Signal Slot Mechanism
class LocationUserManager :public QObject {
    Q_OBJECT  // this had to be set


    private:
        // Inner Properties
        QString                MyIETWikiURL;                             // Where to get All the Information
        QString                MyLocationID;                             // We are here
        QListView             *MyErrorsListViewRef;                      // Here we can show Errors
        QComboBox             *MyUserNamesCombo;                         // Here Users Have to Show up
        QStandardItemModel    *MyUserNamesList;                          // Stores Items For User List
        QStandardItemModel    *MyErrorsList;                             // Hier drin werden die Fehler gesammelt
        QStandardItemModel    *MyInfoTextsList;                          // Hier stehen Statusmeldungen drin
        QNetworkAccessManager *MyAccessManger;
        QNetworkReply         *MyGetStoreUsersRepley;                    // Die Antwort auf eine Netzanfrage nach den Usern im Lager
        LocationAccessObject  *MyGetAccesByRFIDReplyBundle;
        LocationAccessObject  *MyGetAccesByUserNickNameReplyBundle;        
        QTimer                *MyUpdateUserListTimer;

        // Inner Methods
        void                   AddError(QString AnError);
        void                   AddInfo(QString AnInfo);
        void                   ReadListFromServer();

        QString                ReplaceUniCodeChars(QString Input);
        QString                BuildGetStoreUsersURL();
        QString                BuildGetAccessByRFIDURL(QString RFID);
        QString                BuildGetAccesByUserNickNameURL(QString UserNickName);

    private slots:
        void                   OnNetWorkReply(QNetworkReply* aNetWorkreply);
        void                   ReactOnUpdateUserListTimer();

    public:
        void                  AddListViewtoShowErrors(QListView *aListViewToShowErrors);
        void                  AddListViewtoShowInfos(QListView  *aListViewToShowInfos);
        LocationAccessObject *TryToGetAccessByUserNickNameAndPassword(QString UserNickName, QString Password);
        LocationAccessObject *TryToGetAccessByRFID(QString RFID);
        LocationUserManager(QString aLocationID, QComboBox *aComboToShowPossibleLocationUsers);
        ~LocationUserManager();
        QString               IETWikiURL();
        QString               Location();

    signals:
        void OnAccess(LocationAccessObject *anAccessObject);
        void OnError(QString AnErrorMessage);
};
