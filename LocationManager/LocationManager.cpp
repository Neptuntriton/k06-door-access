#include "LocationManager.h"

// Constructor
LocationUserManager::LocationUserManager(QString aLocationID, QComboBox *aComboToShowPossibleLocationUsers){

    // Saving Informations
    MyLocationID        = aLocationID;
    MyUserNamesCombo    = aComboToShowPossibleLocationUsers;

    // Initiating Inner Classes
    // MyIETWikiURL        = QString("http://141.30.39.38:4000");
    // MyIETWikiURL        = QString("http://141.30.243.39:4000");
    MyIETWikiURL        = QString("http://wiki.iet.mw.tu-dresden.de:4000");

    // List of UserNames
    MyUserNamesList     = new QStandardItemModel();
    MyInfoTextsList     = new QStandardItemModel();
    MyErrorsList        = new QStandardItemModel();

    //
    MyGetStoreUsersRepley               = NULL;
    MyGetAccesByRFIDReplyBundle         = NULL;
    MyGetAccesByUserNickNameReplyBundle = NULL;


    //  Building the Manager
    MyAccessManger      = new QNetworkAccessManager(NULL);
    // Connectin An Action on it
    QObject::connect(MyAccessManger, SIGNAL(finished(QNetworkReply*)), this, SLOT(OnNetWorkReply(QNetworkReply*)));

    MyUserNamesCombo->setModel(MyUserNamesList);

    AddInfo("LocationUserManager is up and running.");

    ReadListFromServer();

    // We start build the Timer and Connect it to the Event
    MyUpdateUserListTimer = new QTimer();
    QObject::connect(MyUpdateUserListTimer, SIGNAL(timeout()), this, SLOT(ReactOnUpdateUserListTimer()));
    MyUpdateUserListTimer->start(120000);

}


// Destructor
LocationUserManager::~LocationUserManager(){
    delete MyAccessManger;
    delete MyUserNamesList;
    delete MyErrorsList;
    delete MyInfoTextsList;
}

void LocationUserManager::ReactOnUpdateUserListTimer(){
    ReadListFromServer();
}

// Hier holen wir uns nun den Stoff vom Server
void LocationUserManager::ReadListFromServer(){
    MyErrorsList->clear();
    // Hier muss nun der Call zum Server ausgeführt werden!
    MyGetStoreUsersRepley = MyAccessManger->get(QNetworkRequest(QUrl(BuildGetStoreUsersURL())));
}

// Wir prüfen den Zugang über Username And Password
LocationAccessObject *LocationUserManager::TryToGetAccessByUserNickNameAndPassword(QString UserNickName, QString Password){
    MyErrorsList->clear();
    MyGetAccesByUserNickNameReplyBundle = new LocationAccessObject(UserNickName, Password,  MyAccessManger->get(QNetworkRequest(QUrl(BuildGetAccesByUserNickNameURL(UserNickName)))));
    return MyGetAccesByUserNickNameReplyBundle;
}

// Wir prüfen den Zugang über eine RFID Nummer
LocationAccessObject *LocationUserManager::TryToGetAccessByRFID(QString RFID){
    MyErrorsList->clear();
    MyGetAccesByRFIDReplyBundle = new LocationAccessObject(RFID,  MyAccessManger->get(QNetworkRequest(QUrl(BuildGetAccessByRFIDURL(RFID)))));
    return MyGetAccesByRFIDReplyBundle;
}


// We add an Info to our internal ErrorList And Try to Call an ErrorHandler
void  LocationUserManager::AddInfo(QString AnInfo){
    MyInfoTextsList->appendRow(new QStandardItem(QTime::currentTime().toString() + " Info: " + AnInfo));
}

// We add an Error to our internal ErrorList And Try to Call an ErrorHandler
void LocationUserManager::AddError(QString AnError){
    QString ErrorMessage = QTime::currentTime().toString() + " Error: " + AnError;
    MyErrorsList->appendRow(new QStandardItem(ErrorMessage));
    emit OnError(ErrorMessage);
}


void LocationUserManager::AddListViewtoShowErrors(QListView *aListViewToShowErrors) {
    if (aListViewToShowErrors != NULL){
        aListViewToShowErrors->setModel(MyErrorsList);
    }
}

void LocationUserManager::AddListViewtoShowInfos(QListView  *aListViewToShowInfos) {
    if (aListViewToShowInfos != NULL)  {
        aListViewToShowInfos->setModel(MyInfoTextsList);
    }
}


// Returns an URl
QString LocationUserManager::IETWikiURL(){
    return MyIETWikiURL;
}


// Wir bauen den AbfrageString zu einer URl sammen
// Bsp.: https://storage.iet.mw.tu-dresden.de/getStoreUsers/YTrmyMiD8Cmp7ydQJ
// Bsp.: http://141.30.243.39:4000/getStoreUsers/YTrmyMiD8Cmp7ydQJ

QString LocationUserManager::BuildGetStoreUsersURL(){
    return MyIETWikiURL + QString("/getStoreUsers/") + MyLocationID;
}

// Wir bauen den AbfrageString zu einer URl sammen
// Bsp.: https://storage.iet.mw.tu-dresden.de/getStoreAccess/YTrmyMiD8Cmp7ydQJ/withNickname/Neptuntriton
// Bsp.: http://141.30.243.39:4000/getStoreAccess/YTrmyMiD8Cmp7ydQJ/withNickname/Neptuntriton

QString LocationUserManager::BuildGetAccesByUserNickNameURL(QString UserNickName){
    return MyIETWikiURL + QString("/getStoreAccess/") + MyLocationID + QString("/withNickname/") + UserNickName;
}

// Wir bauen den AbfrageString zu einer URl sammen
// Bsp.: https://storage.iet.mw.tu-dresden.de/getStoreAccess/YTrmyMiD8Cmp7ydQJ/withTag/04186399C12580
// Bsp.: https://141.30.243.39:4000/getStoreAccess/YTrmyMiD8Cmp7ydQJ/withTag/04186399C12580

QString LocationUserManager::BuildGetAccessByRFIDURL(QString RFID){
    return MyIETWikiURL + QString("/getStoreAccess/") + MyLocationID + QString("/withTag/") + RFID;
}


// Return Location
QString LocationUserManager::Location(){
    return MyLocationID;
}


// is called on a NetWork Reply
void LocationUserManager::OnNetWorkReply(QNetworkReply *aNetWorkreply){
    bool          OK;
    QJson::Parser MyParser;
    QVariantMap   MyParsedResult;
    AddInfo("OnNetWorkReply called.");

    // Auf welche Frage wurde denn geantwortet
    if ((MyGetStoreUsersRepley != NULL) && (aNetWorkreply == MyGetStoreUsersRepley)){
        AddInfo("|-> GetStoreUsersRepley.");
        // We want to parse the Repley here
        if (aNetWorkreply->error() == QNetworkReply::NoError){
            MyParsedResult = MyParser.parse(aNetWorkreply->readAll(), &OK).toMap();
            if (OK){
                AddInfo("|-> JSON parsed");
                if (MyParsedResult.count("StoreUsers") == 1){
                    AddInfo("|-> StoreUsers Key found.");
                    AddInfo("|-> KeyCount" + QString::number(MyParsedResult.count("StoreUsers")));
                    MyUserNamesList->clear();
                    for (int a = 0; a < MyParsedResult["StoreUsers"].toList().count(); a++){
                        QVariantMap userEntry = MyParsedResult["StoreUsers"].toList().at(a).toMap();
                        MyUserNamesList->appendRow(new QStandardItem(userEntry["Nickname"].toString() + " | " + userEntry["RealName"].toString()));
                    }
                }
                if (MyParsedResult.count("Errors") == 1){
                    AddInfo("|-> Errors Key found.");
                    foreach (QVariant AnError, MyParsedResult["Errors"].toList()){
                        AddError(AnError.toString());
                    }
                }
            }
        } else {
           AddError("Http Error. Code: " + aNetWorkreply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toString());
        }
    }

    if ((MyGetAccesByRFIDReplyBundle != NULL) && (aNetWorkreply == MyGetAccesByRFIDReplyBundle->NetworkReply())){
        AddInfo("|-> GetAccessByRFIDReply.");
        if (aNetWorkreply->error() == QNetworkReply::NoError){
            MyParsedResult = MyParser.parse(aNetWorkreply->readAll(), &OK).toMap();
            if (OK){
                if (MyParsedResult.count("Access") == 1){
                    if (MyParsedResult["Access"].toString() == "Access granted!"){
                        AddInfo("Access granted for RFID: " + MyGetAccesByRFIDReplyBundle->RFID());
                        MyGetAccesByRFIDReplyBundle->ActivateAccess();
                    }
                }
                if (MyParsedResult.count("Errors") == 1){
                    AddInfo("|-> Errors Key found.");
                    foreach (QVariant AnError, MyParsedResult["Errors"].toList()){
                        AddError(AnError.toString());
                    }
                }
            } else {
                AddError("JSON Not parseable.");
            }
        } else {
            AddError("Http Error. Code: " + aNetWorkreply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toString());
        }
        emit OnAccess(MyGetAccesByRFIDReplyBundle);
    }

    if ((MyGetAccesByUserNickNameReplyBundle != NULL) && (aNetWorkreply == MyGetAccesByUserNickNameReplyBundle->NetworkReply())){
        AddInfo("|-> GetAccesByUserNickNameReplay.");
        if (aNetWorkreply->error() == QNetworkReply::NoError){
            MyParsedResult = MyParser.parse(aNetWorkreply->readAll(), &OK).toMap();
            if (OK){
                // We have to Analyse the Hash
                if (MyParsedResult.count("WikiUserIDOrSalt") == 1){
                    QString IDOrSalt = MyParsedResult["WikiUserIDOrSalt"].toString();
                    QString UserHash = MyParsedResult["WikiUserHash"].toString();
                    QString Encoded  = QString::fromStdString(md5(IDOrSalt.toStdString() + "-" + md5(MyGetAccesByUserNickNameReplyBundle->PassWord().toStdString())));
                    if (Encoded == UserHash){
                        // here we have emit an Signal! With the LocationAccessObject
                        AddInfo("Access granted to User: " + MyGetAccesByUserNickNameReplyBundle->UserNickName());
                        MyGetAccesByUserNickNameReplyBundle->ActivateAccess();
                    } else {
                        AddError("Access denied! Wrong Password!");
                    }
                }
                if (MyParsedResult.count("Errors") == 1){
                    AddInfo("|-> Errors Key found.");
                    foreach (QVariant AnError, MyParsedResult["Errors"].toList()){
                        AddError(AnError.toString());
                    }
                }
            } else {
                AddError("JSON Not parseable.");
            }
        } else {
            AddError("Http Error. Code: " + aNetWorkreply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toString());
        }
        emit OnAccess(MyGetAccesByUserNickNameReplyBundle);
    }
}



// Some Stupid Stuff to replace common UTF Escaped Characters
QString LocationUserManager::ReplaceUniCodeChars(QString Input){
    Input = Input.replace("\\u00c0", QString::fromUtf8("À"));
    Input = Input.replace("\\u00c1", QString::fromUtf8("Á"));
    Input = Input.replace("\\u00c2", QString::fromUtf8("Â"));
    Input = Input.replace("\\u00c3", QString::fromUtf8("Ã"));
    Input = Input.replace("\\u00c4", QString::fromUtf8("Ä"));
      Input = Input.replace("\\u00c5", QString::fromUtf8("Å"));
      Input = Input.replace("\\u00c6", QString::fromUtf8("Æ"));
      Input = Input.replace("\\u00c7", QString::fromUtf8("Ç"));
      Input = Input.replace("\\u00c8", QString::fromUtf8("È"));
      Input = Input.replace("\\u00c9", QString::fromUtf8("É"));
      Input = Input.replace("\\u00ca", QString::fromUtf8("Ê"));
      Input = Input.replace("\\u00cb", QString::fromUtf8("Ë"));
      Input = Input.replace("\\u00cc", QString::fromUtf8("Ì"));
      Input = Input.replace("\\u00cd", QString::fromUtf8("Í"));
      Input = Input.replace("\\u00ce", QString::fromUtf8("Î"));
      Input = Input.replace("\\u00cf", QString::fromUtf8("Ï"));
      Input = Input.replace("\\u00d1", QString::fromUtf8("Ñ"));
      Input = Input.replace("\\u00d2", QString::fromUtf8("Ò"));
      Input = Input.replace("\\u00d3", QString::fromUtf8("Ó"));
      Input = Input.replace("\\u00d4", QString::fromUtf8("Ô"));
      Input = Input.replace("\\u00d5", QString::fromUtf8("Õ"));
      Input = Input.replace("\\u00d6", QString::fromUtf8("Ö"));
      Input = Input.replace("\\u00d8", QString::fromUtf8("Ø"));
      Input = Input.replace("\\u00d9", QString::fromUtf8("Ù"));
      Input = Input.replace("\\u00da", QString::fromUtf8("Ú"));
      Input = Input.replace("\\u00db", QString::fromUtf8("Û"));
      Input = Input.replace("\\u00dc", QString::fromUtf8("Ü"));
      Input = Input.replace("\\u00dd", QString::fromUtf8("Ý"));
      Input = Input.replace("\\u00df", QString::fromUtf8("ß"));
      Input = Input.replace("\\u00e0", QString::fromUtf8("à"));
      Input = Input.replace("\\u00e1", QString::fromUtf8("á"));
      Input = Input.replace("\\u00e2", QString::fromUtf8("â"));
      Input = Input.replace("\\u00e3", QString::fromUtf8("ã"));
      Input = Input.replace("\\u00e4", QString::fromUtf8("ä"));
      Input = Input.replace("\\u00e5", QString::fromUtf8("å"));
      Input = Input.replace("\\u00e6", QString::fromUtf8("æ"));
      Input = Input.replace("\\u00e7", QString::fromUtf8("ç"));
      Input = Input.replace("\\u00e8", QString::fromUtf8("è"));
      Input = Input.replace("\\u00e9", QString::fromUtf8("é"));
      Input = Input.replace("\\u00ea", QString::fromUtf8("ê"));
      Input = Input.replace("\\u00eb", QString::fromUtf8("ë"));
      Input = Input.replace("\\u00ec", QString::fromUtf8("ì"));
      Input = Input.replace("\\u00ed", QString::fromUtf8("í"));
      Input = Input.replace("\\u00ee", QString::fromUtf8("î"));
      Input = Input.replace("\\u00ef", QString::fromUtf8("ï"));
      Input = Input.replace("\\u00f0", QString::fromUtf8("ð"));
      Input = Input.replace("\\u00f1", QString::fromUtf8("ñ"));
      Input = Input.replace("\\u00f2", QString::fromUtf8("ò"));
      Input = Input.replace("\\u00f3", QString::fromUtf8("ó"));
      Input = Input.replace("\\u00f4", QString::fromUtf8("ô"));
      Input = Input.replace("\\u00f5", QString::fromUtf8("õ"));
      Input = Input.replace("\\u00f6", QString::fromUtf8("ö"));
      Input = Input.replace("\\u00f8", QString::fromUtf8("ø"));
    Input = Input.replace("\\u00f9", QString::fromUtf8("ù"));
    Input = Input.replace("\\u00fa", QString::fromUtf8("ú"));
    Input = Input.replace("\\u00fb", QString::fromUtf8("û"));
    Input = Input.replace("\\u00fc", QString::fromUtf8("ü"));
    Input = Input.replace("\\u00fd", QString::fromUtf8("ý"));
    Input = Input.replace("\\u00ff", QString::fromUtf8("ÿ"));
    return Input;
}

// Some HelperClass
QNetworkReply *LocationAccessObject::NetworkReply(){
    return MyNetWorkRepley;
}

QString LocationAccessObject::PassWord(){
    return MyRelatedPassword;
}

QString LocationAccessObject::UserNickName(){
    return MyRelatedUserNickName;
}

QString LocationAccessObject::RFID(){
    return MyRelatedRFID;
}

bool LocationAccessObject::HasAccess(){
    return MyAccessState;
}


void LocationAccessObject::ActivateAccess(){
    MyAccessState = true;
}

LocationAccessObject::LocationAccessObject(QString UserNickName, QString PassWord, QNetworkReply *NetWorkRepley){
    MyAccessState          = false;
    MyRelatedUserNickName  = UserNickName;
    MyRelatedPassword      = PassWord;
    MyNetWorkRepley        = NetWorkRepley;
}

LocationAccessObject::LocationAccessObject(QString RFID, QNetworkReply *NetWorkRepley){
    MyAccessState          = false;
    MyRelatedRFID          = RFID;
    MyNetWorkRepley        = NetWorkRepley;
}



