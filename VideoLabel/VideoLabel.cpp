#include "VideoLabel.h"

// Der Konstruktor
cVideoLabel::cVideoLabel(QWidget *parent, const QString &GifFileName) : QLabel(GifFileName, parent){
    // We take care of us
    this->setMouseTracking(true);
    this->setGeometry(0, 0, 320, 240);
    MyMovieObject = new QMovie(GifFileName);
    this->setMovie(MyMovieObject);
    MyMovieObject->start();
}

void cVideoLabel::LoadNewAnimation(QString FileName){
    MyMovieObject->stop();
    MyMovieObject->setFileName(FileName);
    MyMovieObject->start();
}

// Here we trigger the MousePress Event
void cVideoLabel::mousePressEvent(QMouseEvent *anEvent){
    emit OnClick();
}
