#include <QLabel>
#include <QObject>
#include <QMovie>


class cVideoLabel : public QLabel {
    Q_OBJECT

    public:
        QMovie *MyMovieObject;
        cVideoLabel(QWidget *parent = 0, const QString& GifFileName = "");
        void mousePressEvent(QMouseEvent *anEvent);
        void LoadNewAnimation(QString FileName);

    signals:
        void OnClick();

};
